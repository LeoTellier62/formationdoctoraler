---
title: Guide installation R
author: Emilie Poisson Caillault
output: pdf_document
---

Ce document est réalisé à partir d'un fichier écrit en Rmarkdown (extension Rmd). Ce format permet de générer des rapports, présentations en html, pdf, doc ....

Planche utile pour réaliser un document Rmd : [https://www.rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf]

# Outils R et Rstudio

lien tutoriel d'installation : [https://quanti.hypotheses.org/1813]

**Ordre d'installation** :

1. installation de R, suivre les instructions selon votre OS sur le cran : 

- Pour windows, télécharger la dernière version sécurisée sous [http://cran.r-project.org/bin/windows/base/]

- Pour Mac OS X, [http://cran.r-project.org/bin/macosx/].

- Pour Linux, directement en console (indications : [https://cran.r-project.org/bin/linux/])
```{bash, eval=F}
sudo apt-get update
sudo apt-get install r-base r-base-dev
```

2. Installation de Rtools

Rtools est un outil nécessaire sous Windows pour pouvoir utiliser des paquets écrits à partir de code c/c++, Fortran ou autres **sous Windows**.

[https://cran.r-project.org/bin/windows/Rtools/]

3. Installation de Rstudio

à télécharger sur [https://www.rstudio.com/products/rstudio/download/]


## Outils en ligne sans installation 

- coding Groung [https://www.tutorialspoint.com/execute_r_online.php] 
console R, pas d'installation de paquet possible, librairies de bases, interface graphique limitée

- rstudio cloud (*conseillé*)
[https://rstudio.cloud/]
interface rstudio usuelle. limité à 50 projets, limité à 1h d'execution.


# Installation de packages utiles

Dans la variable requiredPackages ajouter vos noms de paquets séparés par des ",".

```{r}
requiredPackages=c("knitr","rmarkdown","cluster")
for (pkg in requiredPackages) {
  
  if (pkg %in% rownames(installed.packages()) == FALSE){
    print(pkg)
    install.packages(pkg)
  }
  if (pkg %in% rownames(.packages()) == FALSE){
    library(pkg, character.only = TRUE)
  }
}
```
