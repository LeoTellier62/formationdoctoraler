---
title: "Series Temporelles"
author: "Poisson Emilie"
date: "13 janvier 2021"
output:
  html_document:
    df_print: paged
  pdf_document: default
---

## Séries Temporelles

Commencer par récupérer le fichier sat.csv contenant des données de chlorophyll-a, turbidité extraites de données satellites. La chla est considérée une réponse à la biomasse phytoplanctonique.

**Q1. Extraire le dataframe associé que vous nommerez df.**

``` {r, echo=T}
rm(list=ls(all=T))
setwd("/home/poisson/1Enseignement/FormationR/") #A modifier : votre repertoire de travail
df=read.csv("sat.csv") #importer ma donnée
head(df) # entete de mon fichier - les premieres lignes
summary(df) # resume statistique de chaque colonne
```

**Identifier les colonnes contenant la date, la chlorophylle et la turbidité.**

Utiliser la fonction grep.

``` {r, echo=T}
nom=colnames(df) #fonction affichant les noms des colonnes
indDate=grep(pattern="date",x=nom, ignore.case=T)
indChl=grep(pattern="^chl",x=nom, ignore.case=T) #je recupere le numero de la colonne commençant par Chl
indTurb=grep(pattern="^turb",x=nom, ignore.case=T)
```

La date est située dans la colonne $`r indDate`$ du dataframe, la chlorophylle colonne $`r indChl`$ et la turbidité colonne $`r indTurb`$.

**Q2. Ne conserver dans df que la clhorophylle, la turbidité et la date que vous aurez transformé en format  POSIXt. **

Il faut tout d'abord identifier le format initial de la date 
``` {r, echo=T}
print(df[1,indDate])
```
Il suffit d'utiliser la fonction strptime et lui spécifier le format annee mois jour le tout collé.
``` {r, echo=T}
df[[indDate]]=strptime(as.character(df[,indDate]),format="%Y%m%d");
head(df,1)
print(df$date[1])
plot(df$date,df$chl.µg.l.)
```
Reste à sélectionner que les colonnes demandées
``` {r, echo=T}
#df=df[,c(indDate, indChl,indTurb)]
#df=df[,-c(3, 5,6)]
df=subset(df,select=c(indDate, indChl,indTurb))
head(df)
```
**Q3. Quelle est la période des données acquises (premier jour et dernier jour disponibles) ?**
``` {r, echo=T}
nom=colnames(df)
indDate=grep(pattern="date",x=nom, ignore.case=T)
indChl=grep(pattern="^chl",x=nom, ignore.case=T)
indTurb=grep(pattern="^turb",x=nom, ignore.case=T)
colnames(df)[c(indDate,indChl,indTurb)]=c("date","chl","turb");
df$date=strptime(format(df$date, "%F"),format="%Y-%m-%d")

minJ=min(df$date)
maxJ=max(df$date)
```
Pour simplifier, j'ai renommé les noms des colonnes. 

Pour connaître les dates min et max des données il faut utiliser les fonctions R car les enregistrements peuvent ne pas être dans l'ordre temporel. Le premier jour est donc le $`r as.character(minJ)`$, le dernier le $`r as.character(maxJ)`$.

**Q4. Afficher l'ensemble des données de Chlorophyll-a avec un axe temporel daté.**

``` {r, echo=T, fig.cap="Chlorophyll by sat"}
plot(df$date,df$chl)
#ggplot2::ggplot(df,aes(x=date,y=chl))+geom_point()
```

**Q5. Les données sont sensées être positives, vérifier les valeurs aberrantes. Corriger au besoin.**
``` {r, echo=T, fig.cap="Chlorophyll corrigé"}
indNeg=which(as.numeric(df$chl)<0);
df$chl[indNeg]=NA;
plot(df$date,df$chl,pch='+',type='b')
```

``` {r, echo=T, fig.cap="Turbidité corrigé"}
indNeg=which(as.numeric(df$turb)<0);
df$turb[indNeg]=NA;
```

Vérification des valeurs aberrantes via les statistiques : 

``` {r, echo=T, fig.cap="boxplot"}
par(mfrow=c(1,2))
    boxplot(df$chl);
    boxplot(df$turb);
```

Les valeurs hors moustaches ont une certaine continuité et ne paraissent pas aberrante.

**Q6. Les séries sont-elles régulières? si non, réaliser une complétion des données par une moyenne glissante sur 5 jours. **

``` {r, echo=T}
vecteurIdeal=seq(minJ,maxJ,by="1 day")
d=as.character(format(vecteurIdeal,"%F"))
diff=setdiff(d,as.character(df$date))
print(diff)
manquant=length(diff)
```
Les séries ne sont pas régulières, il manque $`r manquant`$ dates. On va donc merger les données pour conserver toutes les dates puis remplir les trous par une moyenne mobile sur 5 jours.

Régularisation et suppression des doublons si existant :
``` {r, echo=T}
df2=data.frame(date=d); # creation d'un dataframe à partir du vecteur ideal/complet de date
df$date=as.character(df$date)
#jointure
df1=merge(df2,df,by="date",all=T)
#traitement des doublons
classe=sapply(df1, FUN=class)
numPara=which(classe == "numeric") 
df=aggregate(df1[,numPara],by=list(date=as.character(df1$date)),
                FUN=function(x){ out=NA; if(sum(is.na(x))<length(x))
                {out=max(x,na.rm=T);}; out})
head(df);
df$date=strptime(df$date,format="%Y-%m-%d")
rm(df1,df2,d)
```

Complétion :
```{r, echo=T}
for(n in numPara){
  x=df[,n]
  indNA=which(is.na(x))
  nb.indNA=length(indNA)
  nb.x=length(x)
  xcomplete=x
  delai=5
  for(i in 1:nb.indNA){
    val=indNA[i];
    if(val<=delai){xcomplete[val]=x[val+1];#copie du suivant 
    }else{
      if(val>=(nb.x-delai)){xcomplete[val]=x[nb.x-1]; 
      }else{
        borneMin=max(1,val-delai); 
        borneMax=min(val+delai,nb.x);
        if (sum(is.na(x[borneMin:borneMax]))<(borneMax-borneMin+1)){
        xcomplete[val]=mean(x[borneMin:borneMax],na.rm=T); 
        }
      }
    }
  }
  df[,n]=xcomplete;
}
```

**Afficher les séries régularisées et complétées.**
```{r, echo=T, fig.cap="series régularisées et qq trous complétés"}
plot(df)
```

**Q7. Les séries sont-elles bruitées?**
```{r, echo=T, fig.cap="Etude du bruit", fig.height=6}
par(mfrow=c(1,2))
acf(df$chl,na.action=na.pass,type="correlation",lag.max = 500)
acf(df$turb,na.action=na.pass,type="correlation",lag.max=500)
```
Pas de décroissance forte de l'auto-correlation.

**Q8. Etudier les dynamiques journalières de chaque série indépendamment des années.**

```{r, echo=T, fig.cap="dynamique journaliere", fig.height=6}
par(mfrow=c(1,2))
temps=df$date
jour=format(temps,"%j"); #recuperation du numero de jour entre 1 à 366
boxplot(df$chl~jour,main="Chloro per day",pch='+');
boxplot(df$turb~jour,main="Turbidity per day",pch='+');
```

**Q9. Les séries ont-elles une tendance? Existe-t-il un cycle saisonnier, sur quelle(s) fréquence(s) ?**

```{r, echo=T, fig.cap="decomposition", fig.height=6}
temps1=min(df$date)
temps2=max(df$date)
df1=df

df1$chl[is.na(df$chl)]=0; #pour les na restant
df1$turb[is.na(df$turb)]=0;

chloro=ts(df1$chl,
        start=c(as.numeric(format(temps1,"%Y")),as.numeric(format(temps1,"%j"))),
        frequency=365)
turb=ts(df1$turb,
        start=c(as.numeric(format(temps1,"%Y")),as.numeric(format(temps1,"%j"))),
        end=c(as.numeric(format(temps2,"%Y")),as.numeric(format(temps2,"%j"))),
        frequency=365)
a=decompose(chloro);
b=decompose(turb);

par(mfrow=c(1,2))
plot(a)
plot(b)

```

Les données présentent une cycle annuel mais n'ont pas de tendance globale.

**Q10. Les valeurs extrêmes résiduelles sont-elles à considérer?**

oui. les valeurs ne sont pas négligeables par rapport à l'amplitude du signal.

**Q11. Faire un échantillonnage mensuel de la série Chla en considérant la moyenne, et afficher la courbe de Chla. Analysez la dynamique des valeurs obtenues. Cet échantillonnage est-il pertinent pour détecter des pics de biomasse phytoplanctonique ?**na.rm=T)

```{r, echo=T, fig.cap="mensuel", fig.height=6}
temps=df$date
moisAnnee=format(temps,"%Y-%m")
chloro=df$chl
df.mensuel=aggregate(chloro,by=list(moisAnnee=moisAnnee),FUN=max,na.rm=T)
summary(df.mensuel)
summary(df$chl)
plot(df.mensuel$x,pch='+',type='b')
# a cause de aggregation -> les temps redeviennent des caracteres.
# formatage des temps.
df.mensuel$date=strptime(paste(as.character(df.mensuel$moisAnnee),15,sep="-"),format="%Y-%m-%d")
print(df.mensuel$date[1])

par(mfrow=c(1,2))
plot(df$date,df$chl,type='b')
plot(df.mensuel$date,df.mensuel$x,pch='+',type='b')

```

Cet échantillonnage avec operateur mean est grossier, l'importance des pics de biomasse très élevée est perdue :  ratio 61/26. (conseil : utiliser max ici pour detecter un pic de concentration). Le détail temporel des pics est aussi perdu. 

## generation Latex

```{r}
setwd("/home/poisson/1Enseignement/FormationR/")
#cree un fichier md
knitr::knit("SeriesTemporelles.rmd")
#cree tex
rmarkdown::pandoc_convert("SeriesTemporelles.md", to = "latex", output = "SeriesTemporelles1.tex", option="--standalone")
#
rmarkdown::render("SeriesTemporelles.rmd", output_format = "latex_document")

```

